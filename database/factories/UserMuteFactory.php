<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\UserMute;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserMuteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserMute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $time = $this->faker->dateTimeBetween('-2 days');

        return [
            'expired_at' => $this->faker->dateTimeBetween($time, '1 week'),
            'updated_at' => $time,
            'created_at' => $time,
        ];
    }
}
