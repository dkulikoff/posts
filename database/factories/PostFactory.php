<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $time = $this->faker->dateTimeBetween('-6 days');

        return [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'updated_at' => $time,
            'created_at' => $time,
        ];
    }
}
