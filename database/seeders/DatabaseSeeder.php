<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use App\Models\UserMute;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Throwable;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     * @throws Throwable
     */
    public function run(): void
    {
        Artisan::call('cache:clear');

        $totalUsers = random_int(3, 6);

        User::factory()
            ->has(
                Post::factory()->count(random_int(1, 50))
            )
            ->count($totalUsers)
            ->create();

        for ($i = 0; $i < random_int(1, intdiv($totalUsers, 2)); $i++) {
            $blocker = random_int(1, $totalUsers);

            do {
                $blocked = random_int(1, $totalUsers);
            } while($blocked === $blocker);

            UserMute::factory([
                    'user_id' => $blocker,
                    'mute_id' => $blocked,
                ])
                ->create();
        }
    }
}
