@forelse($posts as $post)
    @include('sub_views.post', [
        'number' => $loop->iteration,
        'post' => $post,
    ])
@empty
    <h3 class="text-center">No posts found.</h3>
@endforelse
