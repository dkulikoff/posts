<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Posts</title>
    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
</head>
<body>
    <div class="container-fluid">
        <div class="row mt-3">
            @yield('controls')
        </div>

        <div id="posts" class="row"></div>
    </div>

    @section('scripts')
        <script src="{{ asset("js/app.js") }}"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    @show
</body>
</html>
