@extends('layout')

@section('controls')
    @if($users->count() > 0)
        <div class="col-2">
            <select id="user-select" class="form-control">
                @foreach($users as $id => $username)
                    <option value="{{ $id }}">{{ $username }}</option>
                @endforeach
            </select>
        </div>
    @else
        <h3>No users found.</h3>
    @endif
@endsection

@section('scripts')
    @parent

    @include('scripts')
@endsection
