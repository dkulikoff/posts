<div class="post col-4">
    <h3>{{ $number }}. {{ $post->title }}</h3>
    <p class="text-muted">{{ $post->updated_at }}</p>
    <h5>{{ $post->username }}</h5>
    <p>{{ $post->body }}</p>
</div>
