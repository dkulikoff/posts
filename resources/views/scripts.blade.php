<script>
    $(document).ready(function () {
        let $postsContainer = $('#posts'),
            $userSelect = $('#user-select');

        getPosts($userSelect.val());

        $userSelect.change(function (e) {
            getPosts($(e.target).val());
        });

        function getPosts(userId) {
            let route = "{{ route('posts', ['user_id' => ":id"]) }}";

            $.ajax({
                url: route.replace(':id', userId),
                dataType: 'html',
                success: (result) => {
                    $postsContainer.html(result);
                }
            });
        }
    });
</script>
