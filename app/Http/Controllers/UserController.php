<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repository\UserRepository;
use Illuminate\Contracts\View\View;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(): View
    {
        return view('main', [
            'users' => $this->repository->getUserInfo(),
        ]);
    }
}
