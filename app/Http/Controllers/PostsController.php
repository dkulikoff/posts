<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Cache\PostsCache;
use App\Models\User;
use App\Repository\PostRepository;
use Illuminate\Contracts\View\View;

class PostsController extends Controller
{
    /**
     * @var PostRepository
     */
    private $repository;

    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    public function openPosts(
        User $user,
        string $sortOrder = PostsCache::SORT_ORDER_LATEST,
        int $limit = PostsCache::CACHED_POSTS
    ): View {
        return view('posts', [
            'posts' => $this->repository->openPosts($user, $sortOrder, $limit),
        ]);
    }
}
