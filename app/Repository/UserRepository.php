<?php

declare(strict_types=1);

namespace App\Repository;

use App\Cache\UsersCache;
use App\Models\User;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository
{
    /**
     * @var UserMutesRepository
     */
    private $userMutesRepository;

    public function __construct(User $model, UsersCache $cache, UserMutesRepository $userMutesRepository)
    {
        parent::__construct($model, $cache);
        $this->userMutesRepository = $userMutesRepository;
    }

    public function getUserInfo(): Collection
    {
        $users = $this->cache->get();

        return $users ?: $this->cacheUserInfo();
    }

    private function cacheUserInfo(): Collection
    {
        $users = $this->model->pluck('username', 'id');
        $this->cache->put($users);

        return $users;
    }
}
