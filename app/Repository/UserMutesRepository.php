<?php

declare(strict_types=1);

namespace App\Repository;

use App\Models\UserMute;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class UserMutesRepository extends BaseRepository
{

    public function __construct(UserMute $model)
    {
        parent::__construct($model);
    }

    public function getActiveBlocks(): Collection
    {
        return $this->model
            ->where('expired_at', '>', Carbon::now())
            ->distinct()
            ->get(['mute_id', 'user_id'])
            ->groupBy('user_id');
    }
}
