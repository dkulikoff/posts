<?php

declare(strict_types=1);

namespace App\Repository;

use App\Cache\PostsCache;
use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PostRepository extends BaseRepository
{

    /**
     * @var UserMutesRepository
     */
    private $userMutesRepository;

    public function __construct(Post $model, PostsCache $cache, UserMutesRepository $userMutesRepository)
    {
        parent::__construct($model, $cache);
        $this->userMutesRepository = $userMutesRepository;
    }

    public function openPosts(User $user, string $sortOrder, int $limit): Collection
    {
        $posts = $this->allPosts($sortOrder, $limit);
        $blockedUserIds = $user->blockedUsers()->pluck('mute_id');

        return $posts
            ->filter(function ($post) use ($blockedUserIds) {
                return !$blockedUserIds->contains($post->user_id);
            })
            ->take($limit);
    }

    public function allPosts(string $sortOrder, int $limit): Collection
    {
        $this->cache->setOrder($sortOrder);
        $posts = $this->cache->get();

        return $posts ? $posts : $this->cachePosts($sortOrder, $limit);
    }

    private function cachePosts(string $sortOrder, int $limit): Collection
    {
        $orderBy = $this->getOrderBy($sortOrder);
        $posts = $this->getPosts($orderBy, $limit)->get();

        if ($limit > $posts->count()) {
            $this->cache->put($posts);

            return $posts;
        }

        $cachedPostAuthors = $posts->pluck('user_id');
        $blocks = $this->userMutesRepository->getActiveBlocks();

        foreach ($blocks as $block) {
            /** @var Collection $blockedIds */
            $blockedIds = $block->pluck('mute_id');
            $blockedPostsInCache = $cachedPostAuthors->intersect($blockedIds);

            if ($blockedPostsInCache->count() > 0) {
                $additionalPosts = $this->getExtraPosts(
                    $blockedIds,
                    $orderBy,
                    $blockedPostsInCache->count(),
                    $posts->last()->updated_at
                );

                $posts = $posts->merge($additionalPosts);
            }
        }

        $this->cache->put($posts);

        return $posts;
    }

    private function getExtraPosts(
        Collection $blocked,
        array $orderBy,
        int $limit,
        string $sinceDate
    ): Collection {
        return $this->getPosts($orderBy, $limit)
            ->whereNotIn('posts.user_id', $blocked)
            ->where($orderBy['column'], $orderBy['direction'] === 'desc' ? '<' : '>', Carbon::parse($sinceDate))
            ->get();
    }

    private function getPosts(array $orderBy, int $limit): Builder
    {
        return DB::table('posts')
            ->select('posts.id', 'user_id', 'title', 'body', 'username', 'posts.updated_at')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->orderBy($orderBy['column'], $orderBy['direction'])
            ->limit($limit);
    }

    private function getOrderBy(string $sortOrder): array
    {
        switch ($sortOrder) {
            case PostsCache::SORT_ORDER_OLDEST:
                return [
                    'column' => 'posts.updated_at',
                    'direction' => 'asc',
                ];
            case PostsCache::SORT_ORDER_LATEST:
            default:
                return [
                    'column' => 'posts.updated_at',
                    'direction' => 'desc',
                ];
        }
    }
}
