<?php

declare(strict_types=1);

namespace App\Repository;

use App\Cache\BaseCache;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var BaseCache|null
     */
    protected $cache;

    public function __construct(Model $model, ?BaseCache $cache = null)
    {
        $this->model = $model;
        $this->cache = $cache;
    }
}
