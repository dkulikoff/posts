<?php

declare(strict_types=1);

namespace App\Observers;

use App\Cache\PostsCache;
use App\Models\UserMute;

class UserMuteObserver
{
    /**
     * @var PostsCache
     */
    private $postsCache;

    public function __construct(PostsCache $postsCache)
    {
        $this->postsCache = $postsCache;
    }

    /**
     * Handle the UserMute "created" event.
     *
     * @param  \App\Models\UserMute  $userMute
     * @return void
     */
    public function created(UserMute $userMute): void
    {
        if (!$userMute->isActive()) {
            return;
        }

        $this->postsCache->forgetAll();
    }

    /**
     * Handle the UserMute "updated" event.
     *
     * @param  \App\Models\UserMute  $userMute
     * @return void
     */
    public function updated(UserMute $userMute): void
    {
        if (!$userMute->isActive()) {
            return;
        }

        if (array_intersect(array_keys($userMute->getChanges()), ['user_id', 'mute_id', 'expired_at'])) {
            $this->postsCache->forgetAll();
        }
    }
}
