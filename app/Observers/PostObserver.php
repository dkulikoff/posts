<?php

declare(strict_types=1);

namespace App\Observers;

use App\Cache\PostsCache;
use App\Models\Post;

class PostObserver
{
    /**
     * @var PostsCache
     */
    private $postsCache;

    public function __construct(PostsCache $postsCache)
    {
        $this->postsCache = $postsCache;
    }

    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        $this->postsCache->forgetAll();
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        $this->postsCache->forgetAll();
    }

    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        $this->postsCache->forgetIfExists($post);
    }
}
