<?php

declare(strict_types=1);

namespace App\Cache;

use App\Models\Post;
use Illuminate\Contracts\Cache\Repository as Cache;

class PostsCache extends BaseCache
{
    public const CACHED_POSTS = 50;
    public const SORT_ORDER_OLDEST = 'oldest';
    public const SORT_ORDER_LATEST = 'latest';

    public const SORT_ORDERS = [
        self::SORT_ORDER_LATEST,
        self::SORT_ORDER_OLDEST,
    ];

    /**
     * @var string
     */
    private $prefix = 'posts';

    public function __construct(Cache $cache)
    {
        parent::__construct($cache, $this->prefix);
    }

    public function setOrder(string $sortOrder): void
    {
        $this->key = $this->prefix . '-' . $sortOrder;
    }

    public function forgetIfExists(Post $post): void
    {
        foreach (self::SORT_ORDERS as $sortOrder) {
            $this->setOrder($sortOrder);
            $posts = $this->get();

            if (!$posts) {
                continue;
            }

            if ($posts->contains('id', $post->id)) {
                $this->forget();
            }
        }
    }

    public function forgetAll(): void
    {
        foreach (self::SORT_ORDERS as $sortOrder) {
            $this->setOrder($sortOrder);
            $this->forget();
        }
    }
}
