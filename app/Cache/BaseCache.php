<?php

declare(strict_types=1);

namespace App\Cache;

use Carbon\Carbon;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\Collection;

abstract class BaseCache
{
    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var string
     */
    protected $key;

    public function __construct(Cache $cache, string $key)
    {
        $this->cache = $cache;
        $this->key = $key;
    }

    /**
     * @return Collection|null|mixed
     */
    public function get()
    {
        return $this->cache->get($this->key);
    }

    public function put($value, ?Carbon $expiration = null): void
    {
        $this->cache->put($this->key, $value, $expiration);
    }

    public function forget(): void
    {
        $this->cache->forget($this->key);
    }
}
