<?php

declare(strict_types=1);

namespace App\Cache;

use Illuminate\Contracts\Cache\Repository as Cache;

class UsersCache extends BaseCache
{

    public function __construct(Cache $cache)
    {
        parent::__construct($cache, 'users');
    }
}
