<?php

namespace App\Providers;

use App\Models\Post;
use App\Models\UserMute;
use App\Observers\PostObserver;
use App\Observers\UserMuteObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        UserMute::observe(UserMuteObserver::class);
        Post::observe(PostObserver::class);
    }
}
